/*
    IPv6 Canvas
    Copyright (C) 2024 Zipdox

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <sys/mman.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <libsoup/soup.h>
#include <glib-unix.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>

#define SERVER_VERSION "0.2"
#define SOCK_RX_BUFSZ (32 * 1024 * 1024)
#define RECV_BATCH_SIZE (4096)

#define FILE_SIZE ((uint64_t)65536 * (uint64_t)65536 * 3 * 8)
#define ROW_SIZE  (256 * 256 * 3 * 256)
#define TILE_SIZE (256 * 256 * 3)

static unsigned char       icmp_type[RECV_BATCH_SIZE];
static struct iovec        iov[RECV_BATCH_SIZE];
static struct sockaddr_in6 srcaddr[RECV_BATCH_SIZE];
static unsigned char       control_buf[RECV_BATCH_SIZE * CMSG_SPACE(sizeof(struct in6_pktinfo))];
static struct mmsghdr      msgs[RECV_BATCH_SIZE];

time_t        tile_modified[256][256]; // Tracks the last time a tile was modified for web caching
GHashTable    *changed_pixels;         // Tracks the pixels that have changed since the last ws update
GByteArray    *ws_buffer;              // Buffer for websocket messages
unsigned char *image;                  // The image data

typedef struct {
    GList *users;
    GMainLoop *loop;
    gboolean run;
} Server;

typedef struct {
    long x1, x2;
    long y1, y2;
    SoupWebsocketConnection *connection;
    Server *server;
} User;

gboolean handle_int(gpointer user_data){
    g_main_loop_quit(user_data);
    return G_SOURCE_REMOVE;
}

gboolean ignore_signal(gpointer user_data){
    return G_SOURCE_CONTINUE;
}

void socket_closed(SoupWebsocketConnection *self, gpointer user_data){
    User *user = user_data;
    user->server->users = g_list_remove(user->server->users, user);
    g_object_unref(self);
    if(!user->server->run && user->server->users == NULL) g_main_loop_quit(user->server->loop);
    g_free(user);
}

gboolean send_user_buffers(gpointer user_data){
    Server *server = user_data;
    GList *user_l = server->users;
    GByteArray *ws_buffer = g_byte_array_new();

    while(user_l){
        User *user = user_l->data;
        SoupWebsocketState ws_state = soup_websocket_connection_get_state(user->connection);
        if(ws_state == SOUP_WEBSOCKET_STATE_CLOSING){
            user_l = user_l->next;
            continue;
        }
        if(ws_state == SOUP_WEBSOCKET_STATE_CLOSED){
            user_l = user_l->next;
            socket_closed(user->connection, server);
            continue;
        }

        GHashTableIter iter;
        gpointer key, value;
        g_hash_table_iter_init(&iter, changed_pixels);
        while(g_hash_table_iter_next(&iter, &key, &value)){
            unsigned char *coords = key;
            unsigned char *pixel = value;
            if(coords[0] >= user->x1 && coords[0] <= user->x2 && coords[2] >= user->y1 && coords[2] <= user->y2){
                unsigned char data[7] = {coords[0], coords[1], coords[2], coords[3], pixel[0], pixel[1], pixel[2]};
                g_byte_array_append(ws_buffer, data, 7);
            }
        }
        if(ws_buffer->len > 0){
            soup_websocket_connection_send_binary(user->connection, ws_buffer->data, ws_buffer->len);
            g_byte_array_set_size(ws_buffer, 0);
        }
        user_l = user_l->next;
    }
    g_byte_array_free(ws_buffer, TRUE);
    g_hash_table_remove_all(changed_pixels);
    return G_SOURCE_CONTINUE;
}

static void init_recv_buffers(void){
    for(size_t ii = 0; ii < RECV_BATCH_SIZE; ii++){
        iov[ii].iov_base = &icmp_type[ii];
        iov[ii].iov_len  = 1;

        msgs[ii].msg_hdr.msg_iov        = &iov[ii];
        msgs[ii].msg_hdr.msg_iovlen     = 1;
        msgs[ii].msg_hdr.msg_name       = &srcaddr[ii];
        msgs[ii].msg_hdr.msg_namelen    = sizeof(srcaddr[ii]);
        msgs[ii].msg_hdr.msg_control    = &control_buf[ii * CMSG_SPACE(sizeof(struct in6_pktinfo))];
        msgs[ii].msg_hdr.msg_controllen = CMSG_SPACE(sizeof(struct in6_pktinfo));
    }
}

unsigned char *coords;
unsigned char *new_colors;
unsigned char *pixel;
gboolean ping_cb(guint fd, GIOCondition condition){
    int msg_count;
    msg_count = recvmmsg(fd, msgs, RECV_BATCH_SIZE, MSG_DONTWAIT, NULL);
    for(int ii = 0; ii < msg_count; ii++){
        if(msgs[ii].msg_len != 1 || icmp_type[ii] != 0x80) continue;

        struct cmsghdr     *cmsg     = CMSG_FIRSTHDR(&msgs[ii].msg_hdr);
        struct in6_pktinfo *pkt_info = (struct in6_pktinfo*) CMSG_DATA(cmsg);
        unsigned char      *addr     = pkt_info->ipi6_addr.s6_addr;

        if(addr[15] == 0) continue; // Ignore transparent pixels

        coords     = &addr[8];
        new_colors = &addr[12];

        long tile_x = coords[0];
        long x = coords[1];
        long tile_y = coords[2];
        long y = coords[3];

        pixel = image + (tile_y * ROW_SIZE + tile_x * TILE_SIZE + 3 * (y * 256 + x));

        if(pixel[0] == new_colors[0] && pixel[1] == new_colors[1] && pixel[2] == new_colors[2] && new_colors[3] == 255) continue;

        if(new_colors[3] == 255){
            memcpy(pixel, new_colors, 3);
        }else{
            unsigned char inv_alpha = 255 - new_colors[3];
            pixel[0] = (new_colors[3] * new_colors[0] + inv_alpha * pixel[0]) >> 8;
            pixel[1] = (new_colors[3] * new_colors[1] + inv_alpha * pixel[1]) >> 8;
            pixel[2] = (new_colors[3] * new_colors[2] + inv_alpha * pixel[2]) >> 8;
        }

        tile_modified[tile_x][tile_y] = time(NULL);
        g_hash_table_insert(changed_pixels, coords, pixel);
    }
    return G_SOURCE_CONTINUE;
}

void socket_message(SoupWebsocketConnection *self, int type, GBytes *message, gpointer user_data){
    User *user = user_data;
    if(!user->server->run) return;

    gsize size = 0;
    const unsigned char *data = g_bytes_get_data(message, &size);
    if(!data) return;
    if(size < 4) return;

    user->x1 = data[0];
    user->x2 = data[1];
    user->y1 = data[2];
    user->y2 = data[3];
}

void socket_cb(SoupServer *soup_server, SoupWebsocketConnection *connection, const char *path, SoupClientContext *client, gpointer user_data){
    Server *server = user_data;
    if(!server->run) return;
    User *user = g_new0(User, 1);
    user->server = server;
    user->connection = g_object_ref(connection);
    g_signal_connect(connection, "closed", G_CALLBACK(socket_closed), user);
    g_signal_connect(connection, "message", G_CALLBACK(socket_message), user);
    server->users = g_list_append(server->users, user);
}

const char *get_mime(const char *path){
    if(g_str_has_suffix(path, ".html")) return "text/html";
    if(g_str_has_suffix(path, ".css")) return "text/css";
    if(g_str_has_suffix(path, ".js")) return "text/javascript";
    return "application/octet-stream";
}
void public_cb(SoupServer *soup_server, SoupMessage *msg, const char *path, GHashTable *query, SoupClientContext *client, gpointer user_data){
    char *real_path = (char*) path;
    if(g_strcmp0(path, "/") == 0) real_path = "/index.html";
    GBytes *resource = g_resources_lookup_data(real_path, G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
    if(!resource) return soup_message_set_status(msg, SOUP_STATUS_NOT_FOUND);
    soup_message_set_status(msg, SOUP_STATUS_OK);
    gsize size = 0;
    gconstpointer data = g_bytes_get_data(resource, &size);
    soup_message_set_response(msg, get_mime(real_path), SOUP_MEMORY_COPY, (const char*) data, size);
    g_bytes_unref(resource);
}

void tile_cb(SoupServer *soup_server, SoupMessage *msg, const char *path, GHashTable *query, SoupClientContext *client, gpointer user_data){
    Server *server = user_data;

    // Check if the server is running
    if(!server->run) return soup_message_set_status(msg, SOUP_STATUS_SERVICE_UNAVAILABLE);
    
    // Check if the tile path is valid
    gchar **path_parts = g_strsplit(path, "/", 0);
    if(path_parts[2] == NULL || path_parts[3] == NULL){
        g_strfreev(path_parts);
        return soup_message_set_status(msg, SOUP_STATUS_BAD_REQUEST);
    }
    long x = strtol(path_parts[2], NULL, 10);
    long y = strtol(path_parts[3], NULL, 10);
    g_strfreev(path_parts);
    if(x < 0 || y < 0 || x > 255 || y > 255){
        return soup_message_set_status(msg, SOUP_STATUS_BAD_REQUEST);
    }

    const char *date_str = soup_message_headers_get_one(msg->request_headers, "If-Modified-Since");
    if(date_str != NULL){
        SoupDate *date = soup_date_new_from_string(date_str);
        time_t date_time = soup_date_to_time_t(date);
        time_t tile_time = tile_modified[x][y];
        soup_date_free(date);
        if(tile_time <= date_time){
            soup_message_set_status(msg, SOUP_STATUS_NOT_MODIFIED);
            return;
        }
    }

    char date_str_i[64];
    strftime(date_str_i, 64, "%a, %d %b %Y %H:%M:%S GMT", gmtime(&tile_modified[x][y]));
    soup_message_set_status(msg, SOUP_STATUS_OK);
    soup_message_headers_append(msg->response_headers, "Last-Modified", date_str_i);
    soup_message_headers_append(msg->response_headers, "Cache-Control", "no-cache max-age=0 must-revalidate");
    soup_message_set_response(msg, "application/octet-stream", SOUP_MEMORY_STATIC, (char *)(image + y * ROW_SIZE + x * TILE_SIZE), TILE_SIZE);
}

int main(int argc, char *argv[]){
    int fd = open("image", O_RDWR | O_CREAT, 0600);
    if(fd == -1){
        perror("Error creating file");
        return EXIT_FAILURE;
    }
    if(ftruncate(fd, FILE_SIZE) != 0){
        perror("Error setting file size");
        close(fd);
        return EXIT_FAILURE;
    }

    image = mmap(NULL, FILE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(image == MAP_FAILED){
        perror("Error mapping file into memory\n");
        close(fd);
        return EXIT_FAILURE;
    }

    if(argc >= 2) if(g_strcmp0(argv[1], "check") == 0){
        printf("Starting checkerboard\n");
        int checkerboard = 0;
        for(int y = 0; y < 256; y++){
            for(int x = 0; x < 256; x++){
                unsigned char *tile = image + (uint64_t)ROW_SIZE * y + TILE_SIZE * x;
                memset(tile, checkerboard * 255, TILE_SIZE);
                checkerboard = !checkerboard;
            }
            checkerboard = !checkerboard;
        }
        printf("Done checkerboarding\n");

        munmap(image, FILE_SIZE);
        close(fd);
        return EXIT_SUCCESS;
    }

    if(argc < 3){
        fprintf(stderr, "Usage: ipv6-canvas [address] [port]\n");
        munmap(image, FILE_SIZE);
        close(fd);
        return EXIT_FAILURE;
    }

    int sock_fd =  socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    if(sock_fd < 0) {
        perror("Ping socket creation failed");
        munmap(image, FILE_SIZE);
        close(fd);
        exit(EXIT_FAILURE);
    }
    int sock_flags = fcntl(sock_fd, F_GETFL, 0);
    if(sock_flags == -1){
        perror("Failed to get socket flags");
        munmap(image, FILE_SIZE);
        close(fd);
        close(sock_fd);
        exit(EXIT_FAILURE);
    }
    sock_flags |= O_NONBLOCK;
    if(fcntl(sock_fd, F_SETFL, sock_flags) == -1){
        perror("Failed to set socket flags");
        munmap(image, FILE_SIZE);
        close(fd);
        close(sock_fd);
        exit(EXIT_FAILURE);
    }

    int on = 1;
    if(setsockopt(sock_fd, IPPROTO_IPV6, IPV6_RECVPKTINFO, &on, sizeof(on)) == -1){
        perror("Ping socket creation failed");
        munmap(image, FILE_SIZE);
        close(fd);
        close(sock_fd);
        exit(EXIT_FAILURE);
    }
    if(setsockopt(sock_fd, SOL_SOCKET, SO_RCVBUFFORCE, &(int){SOCK_RX_BUFSZ}, sizeof(int)) == -1){
        perror("Failed to set receive buffer size");
        munmap(image, FILE_SIZE);
        close(fd);
        close(sock_fd);
        exit(EXIT_FAILURE);
    }

    //initialize changed pixels hash table
    changed_pixels = g_hash_table_new(g_int_hash, g_int_equal);
    ws_buffer = g_byte_array_new();
    time_t current_time = time(NULL);   
    for(int i = 0; i < 256; i++) {
        for(int j = 0; j < 256; j++) {
            tile_modified[i][j] = current_time;
        }
    }

    init_recv_buffers();

    GMainLoop *loop = g_main_loop_new(NULL, FALSE);
    Server server = {NULL, loop, TRUE};

    GSource *ping_source = g_unix_fd_source_new(sock_fd, G_IO_IN);
    g_source_attach(ping_source, NULL);
    g_source_set_callback(ping_source, G_SOURCE_FUNC(ping_cb), NULL, NULL);

    SoupServer *soup_server = soup_server_new("server-header", "IPv6 Canvas/" SERVER_VERSION, NULL);
    soup_server_add_websocket_handler(soup_server, "/ws", NULL, NULL, socket_cb, &server, NULL);
    soup_server_add_handler(soup_server, "/tile/", tile_cb, &server, NULL);
    soup_server_add_handler(soup_server, "/", public_cb, NULL, NULL);

    unsigned long port = strtoul(argv[2], NULL, 10);
    if(port > UINT_MAX) port = UINT_MAX;
    GSocketAddress *address = g_inet_socket_address_new_from_string(argv[1], port);
    if(!address){
        fprintf(stderr, "Error: Invalid address or port\n");
        munmap(image, FILE_SIZE);
        close(fd);
        close(sock_fd);
        return EXIT_FAILURE;
    }

    GError *error = NULL;
    if(!soup_server_listen(soup_server, address, 0, &error)){
        fprintf(stderr, "Error starting server: %s\n", error->message);
        munmap(image, FILE_SIZE);
        close(fd);
        close(sock_fd);
        return EXIT_FAILURE;
    }else{
        GInetAddress *inet_addr = g_inet_socket_address_get_address(G_INET_SOCKET_ADDRESS(address));
        char *add_str = g_inet_address_to_string(inet_addr);
        if(g_inet_address_get_family(inet_addr) == G_SOCKET_FAMILY_IPV6){
            printf("Listening on [%s]:%u\n", add_str, (guint)g_inet_socket_address_get_port(G_INET_SOCKET_ADDRESS(address)));
        }else{
            printf("Listening on %s:%u\n", add_str, (guint)g_inet_socket_address_get_port(G_INET_SOCKET_ADDRESS(address)));
        }
        g_free(add_str);
        g_object_unref(address);
    }

    g_unix_signal_add(SIGINT,  handle_int,    loop);
    g_unix_signal_add(SIGTERM, handle_int,    loop);
    g_unix_signal_add(SIGHUP,  ignore_signal, loop);

    guint update_user_timeout = g_timeout_add(125, send_user_buffers, &server);

    //run main loop
    g_main_loop_run(loop);

    //cleanup
    server.run = FALSE;
    g_source_remove(update_user_timeout);

    g_source_destroy(ping_source);
    g_source_unref(ping_source);

    soup_server_remove_handler(soup_server, "/ws");
    soup_server_remove_handler(soup_server, "/tile/");
    soup_server_remove_handler(soup_server, "/");

    if(server.users != NULL){
        GList *user_l = server.users;
        while(user_l){
            User *user = user_l->data;
            soup_websocket_connection_close(user->connection, SOUP_WEBSOCKET_CLOSE_GOING_AWAY, NULL);
            user_l = user_l->next;
        }
        g_main_loop_run(loop);
    }

    g_object_unref(soup_server);

    if(msync(image, FILE_SIZE, MS_SYNC) == -1)
        perror("Error syncing file");
    if(munmap(image, FILE_SIZE) == -1)
        perror("Error unmapping file");
    close(fd);
    close(sock_fd);

    printf("Synced and closed canvas file\n");

    return EXIT_SUCCESS;
}
