CC=gcc
FLAGS=`pkg-config --cflags libsoup-2.4`
LIBS=`pkg-config --libs libsoup-2.4`
BIN=ipv6-canvas
OPTS=-O3 -march=native -mtune=native -flto
SERVICE_FILE=ipv6-canvas.service
PREFIX=/usr

all: $(BIN)

resources.c: resources.xml
	glib-compile-resources $^ --sourcedir=public --target=$@ --generate-source

resources.o: resources.c
	$(CC) -c -o $@ $^ $(FLAGS) $(OPTS)

main.o: main.c
	$(CC) -c -o $@ $(FLAGS) $^ -Wall $(OPTS)

$(BIN): main.o resources.o
	$(CC) -o $@ $^ $(LIBS) $(OPTS)

clean:
	rm -f *.o resources.c $(BIN)

install_service:
	cp $(SERVICE_FILE) $(PREFIX)/lib/systemd/system/
	systemctl daemon-reload

uninstall_service:
	-systemctl stop $(SERVICE_FILE)
	rm -f $(PREFIX)/lib/systemd/system/$(SERVICE_FILE)
	systemctl daemon-reload

install: all install_service
	mkdir -p /etc/ipv6-canvas
	cp $(BIN) $(PREFIX)/bin

uninstall: uninstall_service
	rm -f $(PREFIX)/bin/$(BIN)

purge: uninstall
	rm -rf /etc/ipv6-canvas
