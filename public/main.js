class canvasManager{
    constructor(canvas){
        // Set up the main canvas
        this.main_canvas = canvas;
        this.main_ctx    = canvas.getContext('2d',{alpha: false});
        this.main_ctx.imageSmoothingEnabled = false;
        this.main_ctx.fillRect(0,0, this.main_canvas.width, this.main_canvas.height);

        // Set up the offscreen canvas
        this.offscreen_canvas = new OffscreenCanvas(this.main_canvas.width, this.main_canvas.height);
        this.offscreen_ctx    = this.offscreen_canvas.getContext('2d',{alpha: false});
        this.offscreen_ctx.imageSmoothingEnabled = false;
        this.offscreen_ctx.fillStyle = 'black';
        this.offscreen_ctx.fillRect(0,0, this.offscreen_canvas.width, this.offscreen_canvas.height);

        // View coordinates
        this.v_x1 = 0;
        this.v_x2 = 0;
        this.v_y1 = 0;
        this.v_y2 = 0;
        this.v_scale = 1;

        // Previous view coordinates
        this.prev_v_x1 = 0;
        this.prev_v_y1 = 0;

        // Tile coordinates
        this.t_x1 = 0;
        this.t_x2 = 0;
        this.t_y1 = 0;
        this.t_y2 = 0;
        this.t_scale = 256;

        this.tile_cache    = new Map();
        this.updated_tiles = new Set();

        this.max_requests     = 10;
        this.running_requests = 0;
        this.queued_tiles     = new Set();
        this.in_viewport      = new Set();

        this.fetch_status = document.getElementById('fetchinfo');
        this.rendering = false;
    }

    // Copy tile bitmaps to offscreen canvas
    renderOffscreenCanvas(moving = false){
        if(this.rendering){
            return;
        }
        this.rendering = true;
        if(moving){
            this.main_ctx.drawImage(this.offscreen_canvas, this.prev_v_x1 - this.v_x1, this.prev_v_y1 - this.v_y1);
            this.rendering = false;
            return;
        }
        this.prev_v_x1 = this.v_x1;
        this.prev_v_y1 = this.v_y1;

        this.tile_cache.forEach((tile, key) => {
            if(!this.in_viewport.has(key)){
                const [x, y] = key.split('/');
                this.offscreen_ctx.drawImage(tile, 
                                             x * this.t_scale - this.v_x1, 
                                             y * this.t_scale - this.v_y1, 
                                             this.t_scale, 
                                             this.t_scale);
                this.in_viewport.add(key);
            }
        });
        this.main_ctx.drawImage(this.offscreen_canvas, 0, 0);
        this.rendering = false;
    }

    // For each tile in queued_tiles, run fetchTile
    // max "max_requests" requests at a time
    fetchTileQueue(){
        while(this.running_requests < this.max_requests && this.queued_tiles.size > 0){
            const tile = this.queued_tiles.values().next().value;
            this.queued_tiles.delete(tile);
            this.running_requests++;
            this.fetchTile(tile.x, tile.y);
        }
        requestAnimationFrame(() => this.renderOffscreenCanvas());
        this.fetch_status.innerText = `Queued: ${this.queued_tiles.size}, Running: ${this.running_requests}`;
    }

    // Retrieve tile data from the server and cache it as an ImageData object
    async fetchTile(x, y){
        const res = await fetch(`/tile/${x}/${y}`).catch(console.error);
        const data = new Uint8Array(await res.arrayBuffer());
        const imgdata = new ImageData(256, 256);
        let africanAmerican = true;
        for(let i = 0; i < 256 * 256; i++){
            const i4 = i * 4;
            const i3 = i * 3;
            if(data[i3] != 0 || data[i3+1] != 0 || data[i3+2] != 0){
                africanAmerican = false;
            }
            imgdata.data[i4]   = data[i3];
            imgdata.data[i4+1] = data[i3+1];
            imgdata.data[i4+2] = data[i3+2];
            imgdata.data[i4+3] = 255;
        }
        if(africanAmerican){
            this.running_requests--;
            this.fetchTileQueue();
            return;
        }
        // Convert the ImageData object to a bitmap
        const bitmap = await createImageBitmap(imgdata);
        this.tile_cache.set(`${x}/${y}`, bitmap);
        this.running_requests--;
        this.fetchTileQueue();
    }

    // Queue tiles that are visible on the screen
    // skip every other tile then fetch the skipped tiles
    //   this should feel faster than fetching all tiles sequentially
    queueTiles(){
        for(let x = this.t_x1; x <= this.t_x2; x++){
            for(let y = this.t_y1; y <= this.t_y2; y++){
                if(!this.tile_cache.has(`${x}/${y}`)){
                    this.queued_tiles.add({x: x, y: y});
                }
            }
        }
    }

    setView(x1, y1, x2, y2, scale, moving = false){
        this.v_x1 = x1;
        this.v_y1 = y1;
        this.v_x2 = x2;
        this.v_y2 = y2;
        this.v_scale = scale;
        this.t_scale = 256 * this.v_scale;
        this.t_x1 = Math.max(0,   Math.floor(this.v_x1 / (this.t_scale)) - 1);
        this.t_y1 = Math.max(0,   Math.floor(this.v_y1 / (this.t_scale)) - 1);
        this.t_x2 = Math.min(255, Math.floor(this.v_x2 / (this.t_scale)) + 1);
        this.t_y2 = Math.min(255, Math.floor(this.v_y2 / (this.t_scale)) + 1);
        this.queued_tiles.clear();
        this.in_viewport.clear();
        if(!moving){
            this.offscreen_ctx.fillStyle = 'black';
            this.offscreen_ctx.fillRect(0,0, this.offscreen_canvas.width, this.offscreen_canvas.height);
        }
        this.main_ctx.fillStyle = 'black';
        this.main_ctx.fillRect(0,0, this.main_canvas.width, this.main_canvas.height);
        this.renderOffscreenCanvas(moving);
    }

    setViewport(){
        this.main_canvas.width = window.innerWidth;
        this.main_canvas.height = window.innerHeight;
        this.offscreen_canvas.width = window.innerWidth;
        this.offscreen_canvas.height = window.innerHeight;
        this.setView(this.v_x1, this.v_y1, this.v_x2, this.v_y2, this.v_scale);
    }

    //clean tiles not visible
    cleanupCache(){
        for(let x = 0; x < 256; x++){
            for(let y = 0; y < 256; y++){
                if(x < this.t_x1 || x > this.t_x2 || y < this.t_y1 || y > this.t_y2){
                    this.tile_cache.delete(`${x}/${y}`);
                }
            }
        }
    }

}


// Init
let cManager = new canvasManager(document.getElementById('canvas'));

cManager.setView(0, 0, window.innerWidth, window.innerHeight, 1);
cManager.setViewport();
cManager.queueTiles();
cManager.fetchTileQueue();

window.addEventListener('resize', () => {
    cManager.setViewport();
    cManager.queueTiles();
    cManager.fetchTileQueue();
});



// Mouse event handlers
const coords = document.getElementById("coords"); // floating label above cursor
let mouse_down = false;
let last_x = 0;
let last_y = 0;
let scale = 1;

document.addEventListener('mousedown', (e) => {
    mouse_down = true;
    last_x = e.clientX;
    last_y = e.clientY;
});
document.addEventListener('mouseup', () => {
    mouse_down = false;
    cManager.setView(cManager.v_x1, cManager.v_y1, cManager.v_x2, cManager.v_y2, scale, false);
    cManager.queueTiles();
    cManager.cleanupCache();
    cManager.fetchTileQueue();
});
document.addEventListener('mousemove', (e) => {
    if(mouse_down){
        const dx = e.clientX - last_x;
        const dy = e.clientY - last_y;
        cManager.setView(cManager.v_x1 - dx, cManager.v_y1 - dy, cManager.v_x2 - dx, cManager.v_y2 - dy, scale, true);
        last_x = e.clientX;
        last_y = e.clientY;
    }
    // Update the coordinates display to show the current mouse position on the full canvas
    coords.innerText = `(${e.clientX + cManager.v_x1}, ${e.clientY + cManager.v_y1})`;
    coords.style.left = `${e.clientX + 10}px`;
    coords.style.top = `${e.clientY + 10}px`;
});
document.addEventListener('wheel', (e) => {
    // Zoom in or out based on the scroll direction
    // Minimum zoom is .25, maximum zoom is 16
    // scale by .1

    const delta = e.deltaY;
    if(delta > 0){
        scale = Math.max(.25, Math.min(16, scale - .01));
    } else {
        scale = Math.max(.25, Math.min(16, scale + .01));
    }

    cManager.setView(cManager.v_x1, cManager.v_y1, cManager.v_x2, cManager.v_y2, scale, false);
    cManager.queueTiles();
    cManager.cleanupCache();
    cManager.fetchTileQueue();

});

//Touch event handlers
let dragStartX, dragStartY;
let pinchStartX, pinchStartY;
let pinchStartDistance;
let activeTouches = 0;
document.addEventListener("touchstart", function(evt) {
    if (evt.touches.length === 1) {
        dragStartX = evt.touches[0].clientX;
        dragStartY = evt.touches[0].clientY;
    } else if (evt.touches.length === 2) {
        pinchStartX = (evt.touches[0].clientX + evt.touches[1].clientX) / 2;
        pinchStartY = (evt.touches[0].clientY + evt.touches[1].clientY) / 2;
        const dx = evt.touches[0].clientX - evt.touches[1].clientX;
        const dy = evt.touches[0].clientY - evt.touches[1].clientY;
        pinchStartDistance = Math.sqrt(dx * dx + dy * dy);
    }
});
document.addEventListener("touchmove", function(evt) {
    if (evt.touches.length === 1) {
        if(activeTouches == 2){
            dragStartX = evt.touches[0].clientX;
            dragStartY = evt.touches[0].clientY;
            activeTouches = 1;
            return;
        }
        // Single touch, drag the canvas
        const moveX = evt.touches[0].clientX;
        const moveY = evt.touches[0].clientY;
        const dX = moveX - dragStartX;
        const dY = moveY - dragStartY;
        dragStartX = moveX;
        dragStartY = moveY;
        cManager.setView(cManager.v_x1 - dX, cManager.v_y1 - dY, cManager.v_x2 - dX, cManager.v_y2 - dY, 1, true);
        cManager.queueTiles();
        cManager.fetchTileQueue();
    
    }
});